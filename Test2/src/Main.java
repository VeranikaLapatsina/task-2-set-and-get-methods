
public class Main {
    public static void main(String[] args) {
        Test2 obj = new Test2();
        System.out.println("a: " + obj.get_a());
        System.out.println("b: " + obj.get_b());
        obj.set_a(123);
        obj.set_b(43);
        System.out.println("a: " + obj.get_a());
        System.out.println("b: " + obj.get_b());
    }
}